package com.example.demo.service;

import com.example.demo.domain.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class StudentService extends AbstractService<Student, StudentRepository> {

    public StudentService(StudentRepository repository) {
        super(repository);
    }

    @Override
    public Student save(Student entity) {
        entity.getDevelopmentPlanItems().forEach(developmentPlanItem -> developmentPlanItem.setStudent(entity));
        return super.save(entity);
    }


    @Override
    public Student update(Student entity) {
        //todo fix delete item case
        return super.update(entity);
    }

    public List<Student> findByName(String name){
        return repository.findByFirstName(name);
    }

    public List<Student> findBySubjectName(String subjectName){
        return repository.findBySubjectName(subjectName);
    }

    public List<Student> throwException() {
       throw new NullPointerException("");
    }
}
