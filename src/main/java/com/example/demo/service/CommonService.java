package com.example.demo.service;

import com.example.demo.domain.common.Resource;

import java.util.List;
import java.util.UUID;

public interface CommonService<E extends Resource> {
    E save(E entity);
    E update(E entity);
    void delete(UUID id);
    E getById(UUID id);
    List<E> getAll();
}
