package com.example.demo.service;

import com.example.demo.domain.Task;
import com.example.demo.repository.TaskRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task, TaskRepository> {

    public TaskService(TaskRepository repository) {
        super(repository);
    }

    public Page<Task> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
