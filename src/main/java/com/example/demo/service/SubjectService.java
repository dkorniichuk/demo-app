package com.example.demo.service;

import com.example.demo.domain.Subject;
import com.example.demo.repository.SubjectRepository;
import org.springframework.stereotype.Service;

@Service
public class SubjectService extends AbstractService<Subject, SubjectRepository> {

    public SubjectService(SubjectRepository repository) {
        super(repository);
    }
}
