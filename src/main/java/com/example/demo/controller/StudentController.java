package com.example.demo.controller;

import com.example.demo.domain.Student;
import com.example.demo.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController extends AbstractController<Student, StudentService> {

    public StudentController(StudentService service) {
        super(service);
    }

    @GetMapping("/name")
    public List<Student> findByName(@RequestParam String name) {
        return service.findByName(name);
    }

    @GetMapping("/subjectName")
    public List<Student> findBySubjectName(@RequestParam String subjectName) {
        return service.findBySubjectName(subjectName);
    }

    @GetMapping("/exception")
    public void throwException() {
         service.throwException();
    }
}
