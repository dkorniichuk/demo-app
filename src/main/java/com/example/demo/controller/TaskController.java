package com.example.demo.controller;

import com.example.demo.domain.Task;
import com.example.demo.service.TaskService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/tasks")
public class TaskController extends AbstractController<Task, TaskService> {

    @Value("${application.pagination-feature}")
    Boolean paginationFeature = true;


    public TaskController(TaskService service) {
        super(service);
    }

    @GetMapping("/pageable")
    public Page<Task> readAll(@PageableDefault(size = 2) Pageable pageable) {
        if (!paginationFeature) throw new NullPointerException();
        return service.findAll(pageable);
    }
}
