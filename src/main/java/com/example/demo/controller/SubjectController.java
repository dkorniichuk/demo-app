package com.example.demo.controller;

import com.example.demo.domain.Subject;
import com.example.demo.service.SubjectService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/subjects")
public class SubjectController extends AbstractController<Subject, SubjectService> {

    public SubjectController(SubjectService service) {
        super(service);
    }
}
