package com.example.demo.domain.type;

public enum TaskType {
    ONLINE, OFFLINE
}
