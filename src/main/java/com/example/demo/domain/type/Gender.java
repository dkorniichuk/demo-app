package com.example.demo.domain.type;

public enum Gender {
    MALE, FEMALE
}
