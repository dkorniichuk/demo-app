package com.example.demo.domain;

import com.example.demo.domain.common.Resource;
import com.example.demo.domain.type.Contact;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.*;

@Entity
@Table
public class Student extends Resource {
    private String firstName;
    private String lastName;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride( name = "city", column = @Column(name = "contact_city")),
            @AttributeOverride( name = "street", column = @Column(name = "contact_street")),
            @AttributeOverride( name = "building", column = @Column(name = "contact_building")),
            @AttributeOverride( name = "phone", column = @Column(name = "contact_phone"))
    })
    private Contact contact;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "profile", referencedColumnName = "id")
    private Profile profile;


    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "student_to_subject",
            joinColumns=@JoinColumn(name= "student_id")
    )
    @Column(name = "subject_id")
    private Set<UUID> subjectIds = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "task",
            joinColumns=@JoinColumn(name= "assignee")
    )
    @Column(name = "id")
    private Set<UUID> taskIds = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("student")
    private List<DevelopmentPlanItem> developmentPlanItems = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String secondName) {
        this.lastName = secondName;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Set<UUID> getSubjectIds() {
        return subjectIds;
    }

    public void setSubjectIds(Set<UUID> subjectIds) {
        this.subjectIds = subjectIds;
    }

    public Set<UUID> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(Set<UUID> taskIds) {
        this.taskIds = taskIds;
    }

    public List<DevelopmentPlanItem> getDevelopmentPlanItems() {
        return developmentPlanItems;
    }

    public void setDevelopmentPlanItems(List<DevelopmentPlanItem> developmentPlanItems) {
        this.developmentPlanItems = developmentPlanItems;
    }
}
