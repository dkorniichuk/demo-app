package com.example.demo.domain;

import com.example.demo.domain.common.Resource;
import com.example.demo.domain.type.TaskType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table
public class Task extends Resource {

    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private TaskType type;
    private UUID assignee;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public UUID getAssignee() {
        return assignee;
    }

    public void setAssignee(UUID assignee) {
        this.assignee = assignee;
    }
}
