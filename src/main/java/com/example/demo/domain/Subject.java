package com.example.demo.domain;

import com.example.demo.domain.common.Resource;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table
public class Subject extends Resource {
    private String name;
    private String description;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "student_to_subject",
            joinColumns=@JoinColumn(name= "subject_id")
    )
    @Column(name = "student_id")
    private Set<UUID> studentIds = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UUID> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(Set<UUID> studentIds) {
        this.studentIds = studentIds;
    }
}
