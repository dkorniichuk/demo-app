package com.example.demo.repository;

import com.example.demo.domain.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends AbstractRepository<Student> {

    List<Student> findByFirstName(String name);


    @Query(nativeQuery = true, value = "SELECT s.* FROM student s INNER JOIN student_to_subject sts ON s.id = sts.student_id " +
            "INNER JOIN subject sbj ON sts.subject_id = sbj.id WHERE sbj.name = :subjectName")
    List<Student> findBySubjectName(String subjectName);


}
