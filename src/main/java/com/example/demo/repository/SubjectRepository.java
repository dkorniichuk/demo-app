package com.example.demo.repository;

import com.example.demo.domain.Subject;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends AbstractRepository<Subject> {
}
