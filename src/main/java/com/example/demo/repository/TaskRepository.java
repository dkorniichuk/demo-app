package com.example.demo.repository;

import com.example.demo.domain.Task;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends AbstractRepository<Task> {
}
