# demo-app

### **Demo plan**

Repository: https://gitlab.com/dkorniichuk/demo-app

1. Intro (API, REST)
2. Business task description
3. Live demo via Postman
4. Technical overview
    1. Spring boot structure - build.gradle, application.yaml, resources
    2. Liquibase as an alternative to Flyway
    3. Domain layer (briefly without hibernate mappings details)
    4. Service layer (briefly)
    5. Controller layer
        1. Mapping, PathVariable, RequestParam, RequestBody, HTTPStatus, ResponseEntity
        2. Exception handling
        3. Pagination
        4. Swagger
    6. Persistence layer
        1. Entity mappings
        2. spring-data-jpa
    7. Spring profiles

#### Useful links:

https://habr.com/ru/post/319984/ 

https://vladmihalcea.com/blog/ 

https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods 

https://www.youtube.com/watch?v=BmBr5diz8WA

https://www.youtube.com/watch?v=cou_qomYLNU

![img.png](img.png)

API: http://localhost:8080/swagger-ui/
